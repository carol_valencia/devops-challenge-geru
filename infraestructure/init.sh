#!/bin/bash

set -e # Exit on first error.
set -x # Print expanded commands to stdout.
# Ensure the instance is up-to-date.
apt-get update -y

  # Install required packages.
apt-get install -y git
apt-get install -y python-pip

# Install Ansible! We use pip as the EPEL package runs on Python 2.6...
export LC_ALL=C
pip install ansible

echo "----------------START-----------------------------"

url="https://github.com/krol3/nginx-ansible-molecule.git"
playbook="default.yml"

ansible-pull --accept-host-key --verbose \
  --url "$url" --directory ~/ansible/bootstrap "$playbook"

echo "----------------END-----------------------------"