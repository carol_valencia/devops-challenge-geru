# default VPC

provider "aws" {
  access_key = "${lookup(var.credential_aws, "access_key")}"
  secret_key = "${lookup(var.credential_aws, "secret_access_key")}"
  region     = "${lookup(var.server, "region")}"
}

data "aws_availability_zones" "all" {}

# AWS auto scaling group (ASG)
resource "aws_autoscaling_group" "ASG_01" {
  launch_configuration = "${aws_launch_configuration.launchConfig_01.id}"
  availability_zones = ["${data.aws_availability_zones.all.names}"]

  min_size = "${lookup(var.server_autoscale, "min")}"
  max_size = "${lookup(var.server_autoscale, "max")}"

  load_balancers = ["${aws_elb.ELB_01.name}"]
  health_check_type = "ELB"

  tag {
    key = "Name"
    value = "${lookup(var.server, "name")}"
    propagate_at_launch = true
  }
}

# The launch configuration in AWS related to ASG
resource "aws_launch_configuration" "launchConfig_01" {
  # ubuntu16 with ansible AMI"
  image_id = "${lookup(var.server, "ami_id")}"
  instance_type = "${lookup(var.server, "instance_type")}"
  security_groups = ["${aws_security_group.server_sg_01.id}"]
  key_name = "${lookup(var.server, "ssh_key_name")}"
  user_data = "${file("${path.module}/init.sh")}"
  name = "${lookup(var.server, "name")}-launch-config"

  lifecycle {
    create_before_destroy = true
  }
}

# The security group SG for the EC2
resource "aws_security_group" "server_sg_01" {
  name = "${lookup(var.server, "name")}-sg"

  # Inbound HTTP from anywhere
  ingress {
    from_port = "${lookup(var.server, "port01")}"
    to_port = "${lookup(var.server, "port01")}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = "${lookup(var.server, "port02")}"
    to_port = "${lookup(var.server, "port02")}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

# The load balancer LB to the ASG
resource "aws_elb" "ELB_01" {
  name = "${lookup(var.server, "name")}-lb"
  security_groups = ["${aws_security_group.lb_sg_01.id}"]
  availability_zones = ["${data.aws_availability_zones.all.names}"]

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:${lookup(var.server, "port01")}/"
  }

  # This adds a listener for incoming HTTP requests.
  listener {
    lb_port = "${lookup(var.server, "port01")}"
    lb_protocol = "http"
    instance_port = "${lookup(var.server, "port01")}"
    instance_protocol = "http"
  }
}

# The security group SG for the LB
resource "aws_security_group" "lb_sg_01" {
  name = "${lookup(var.server, "name")}-sg-lb"

  # Allow all outbound
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Inbound HTTP from anywhere
  ingress {
    from_port = "${lookup(var.server, "port01")}"
    to_port = "${lookup(var.server, "port01")}"
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}