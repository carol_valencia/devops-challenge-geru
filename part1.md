# Build the image: container folder

docker build -t krol/python-flask:1.0 .

docker run -ti --rm \
  --env GERU_PASS=teste123 \
  -p 80:80 --name teste \
  krol/python-flask:1.0

curl -H "Authorization: Token teste123" http://localhost/

# Configuration management: ansible folder
molecule init role --role-name docker --driver-name docker

cd ../devops-challenge-geru/configManagement/docker
molecule test

# Infrastructure: terraform folder
settings in vars.tf

- terraform plan
- terraform apply