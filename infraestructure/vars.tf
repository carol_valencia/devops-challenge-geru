
variable "server" {
  type = "map"
  default = {
    name = "server-krol"
    port01 = 80
    port02 = 22
    ssh_key_name = "sample-keypair"
    instance_type = "t2.micro"
    ami_id = "ami-e84d628d"
    region = "us-east-2"
  }
}

variable "server_autoscale" {
  type = "map"
  default = {
    min = 1
    max = 2
  }
}

variable "credential_aws" {
  type = "map"
  default = {
    access_key = "XXXXXXXXXXX"
    secret_access_key = "XXXXXXXXX"
  }
}