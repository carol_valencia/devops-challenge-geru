
1) aplicação web "www.meusite.com.br" usada na rede da internet.

2) Serviço cloud front, melhora a velocidade de distribuição do conteuto web: statico e dinamico.
Mediante diversos data centers geo localizados.

3) Serviço S3, armazena data e pode trabalhar em sincronia com o serviço cloud front.

4) Serviço API Gateway, facilita a publicação, criação, monitoramento e seguradida para gerencia API's

5) Serviço Lambda: Permite executar as aplicações chamadas de "serverless", com a caracteristica de pagar
só pelo tempo do consumo da execução do programa. Interatua com outros serviços AWS tal como o serviço
API gateway.

6) Serviço dynamoDB: É o banco NoSql na AWS, também interatua com outros serviços para formar um stack de alguma solução completa.

7) VPC: a chamada Amazon Virtual Cloud, provisiona uma rede virtual isolada na nuvem.
Contem toda a abstração de uma rede fisica: subredes, ip, routing, firework, etc.

Resumindo a arquitectura da aplicação "meusite", ela é uma aplicação web,
usa cloud front para melhorar a performance e disponibilidade do site.
Da parte backend da aplicação, utiliza API gerenciada pelo serviço API Gateway,
alguns dos endpoints utilizam os serviços Lambda para se conectar com o banco NoSql.
E a parte do backend encontra-se isolada numa VPC.

